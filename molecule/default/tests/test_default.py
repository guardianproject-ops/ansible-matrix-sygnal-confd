import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/var/lib/sygnal",
            "/var/log/sygnal",
            "/var/run/sygnal",
            "/usr/local/sygnal",
            "/etc/sygnal",
            "/etc/confd",
            "/etc/confd/conf.d",
            "/etc/confd/templates",
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory
        assert d.exists


def test_files(host):
    files = [
            "/etc/sygnal/gunicorn_config.py",
            "/etc/systemd/system/gunicorn.service",
            "/etc/confd/templates/sygnal.conf.tmpl",
            "/etc/confd/conf.d/sygnal.conf.toml",
            "/etc/logrotate.d/sygnal",
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

    f = host.file("/usr/local/bin/sygnal-key-fetcher")
    assert f.exists
    assert f.mode == 0o755


def test_service(host):
    s = host.service("gunicorn")
    assert not s.is_running
    # This is not working..
    # assert s.is_enabled


def test_socket(host):
    sockets = [
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
